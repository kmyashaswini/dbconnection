package com.dbconnection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class UpdatedDbDemo {
	public static void main(String[] args) throws SQLException {
		UpdatedDbDemo demo = new UpdatedDbDemo();
		demo.setUp();
		demo.insertData();
	}
	   private Connection connection;
	   private static Statement statement;
	   private static ResultSet rs;
	   List<Integer> data = new ArrayList<Integer>();
	   public void setUp() {
           String databaseURL ="jdbc:mysql://localhost:3306/employee";
           String user = "root";
           String password = "sharathchethu";
           connection = null;
           try {
               Class.forName("com.mysql.jdbc.Driver");
               System.out.println("Connecting to Database...");
               connection = DriverManager.getConnection(databaseURL, user, password);
               if (connection != null) {
                   System.out.println("Connected to the Database...");
               }
           } catch (SQLException ex) {
              ex.printStackTrace();
           }
           catch (ClassNotFoundException ex) {
              ex.printStackTrace();
           }
   }
	   public void insertData() throws SQLException {
		   String Query = "Insert into empinfo(EmpId,EmpName,Designation)values(5,'chethu','engineer')";
				statement = connection.createStatement();
				statement.execute(Query);
	   
	   }
	   
	   public List<Integer> getData() {
		   try {
	            String query = "select * from empinfo";
	            statement = connection.createStatement();
	            rs = statement.executeQuery(query);

	            while(rs.next()){
	                int EmpId= rs.getInt("EmpId");
	                String EmpName= rs.getString("EmpName");
	                String Designation=rs.getString("Designation");
	                System.out.println(EmpId+"\t"+EmpName+"\t"+Designation);
	                data.add(EmpId);
	            }
	        } catch (SQLException ex) {
	           ex.printStackTrace();
	        }
        return data;
	   }
}